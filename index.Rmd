---
title: 'Géo-calcul et apprentissage <br>statistique (avec `r icon::fa_r_project(colour = "#384CB7")`)<br>exemples et éléments de discussion'
subtitle: 
author: "Cyrille Conord<br>"
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  xaringan::moon_reader:
    lib_dir: libs
    css: ["default", "scrollable.css", "metropolis", "metropolis-fonts", "gentle-ggplot2.css", 'hygge'] # Vector of css paths. Order matters!
    df_print: kable
    nature:
      beforeInit: ["addons/macros.js", "https://platform.twitter.com/widgets.js"]
      ratio: 16:9
      highlightStyle: "foundation"
      highlightLines: true                                                  # Highlight code lines, ie allow #<<
      countIncrementalSlides: true
      slideNumberFormat: "%current%"
editor_options: 
  chunk_output_type: console
---

```{r, include=FALSE,eval=FALSE, echo=FALSE}
# ##======== Sources ========##
#   # macros js pour taille % de images en syntaxe markdown
#     https://yongfu.name/2019/04/29/xaringan_tips.html
#     https://github.com/yihui/xaringan/issues/80
# 
# .font200[
# .text-black[ **Relation entre Décision Multicritères et** ]
# ]
# .font200[
# .text-black[ **Apprentissage Machine** ]
# ]


# <!-- class: fullscreen, inverse, top, center, text-white -->
# <!-- background-image: url("images/VisionMachine_dark.jpg.png") -->
# <!-- background-size: 100% -->
# 
# <!-- .font200[ -->
# <!-- ** Geo-calcul avec `r icon::fa_r_project(colour = "#384CB7")` ** -->
# 
# <!-- ![:scale 30%](images/geocomputation.png) -->
# <!-- ] -->
```

```{r echo=FALSE}
library(icon)
```

```{css, echo = FALSE}
# https://community.rstudio.com/t/using-multiple-font-sizes-for-code-chunks/26405/6
.remark-slide-content {
  font-size: 28px;
  padding: 20px 80px 20px 80px;
}
.remark-code, .remark-inline-code {
  background: #f0f0f0;
}
.remark-code {
  font-size: 24px;
}
.huge .remark-code { /*Change made here*/
  font-size: 200% !important;
}
.tiny .remark-code { /*Change made here*/
  font-size: 50% !important;
}
```

```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.width=4.25, fig.height=3.5, fig.retina=3,
                      message=FALSE, warning=FALSE, cache = TRUE, 
                      autodep = TRUE, hiline=TRUE)
knitr::opts_hooks$set(fig.callout = function(options) {
  if (options$fig.callout) {
    options$echo <- FALSE
    options$out.height <- "99%"
    options$fig.width <- 16
    options$fig.height <- 8
  }
  options
})
hook_source <- knitr::knit_hooks$get('source')
knitr::knit_hooks$set(source = function(x, options) {
  if (!is.null(options$hiline) && options$hiline) {
    x <- stringr::str_replace(x, "^ ?(.+)\\s?#<<", "*\\1")
  }
  hook_source(x, options)
})
options(htmltools.dir.version = FALSE, width = 90)
as_table <- function(...) knitr::kable(..., format='html', digits = 3)
```

```{r, load_refs, include=FALSE, cache=FALSE}
library(RefManageR) #https://cran.r-project.org/web/packages/RefManageR/vignettes/manual.pdf
BibOptions(check.entries = FALSE,
           bib.style = "numeric",
           cite.style = "alphabetic",
           style = "markdown",
           hyperlink = FALSE,
           dashed = FALSE)
myBib <- ReadBib("./refsBib/mesRefsBib", check = FALSE)
```

# Geocomputation & statistical learning 
.pull-left[
![](images/geocomputation.png)]


.pull-right[
.content-box-blue[
"***Statistical learning is concerned with the use of statistical and computational models for identifying patterns in data and predicting from these patterns.***" `r AutoCite(myBib, "Lovelace2019", .opts = list(cite.style = 'alphabetic'))`]
.tiny[
"*Approche plus créative, expérimentale et basée sur la création d'outils que la géographie quantitative.*"
- étroitement liée aux SIG
- mais en mode console
- basée sur des *Read–Eval–Print Loop*
- emphase sur la **reproductibilité**
]] .footnote[<https://geocompr.robinlovelace.net/>]]



<!-- --- -->
<!-- # DMC: rappels -->

<!-- Multicriteria decision aid (MCDA) =providing decision supporting: -->

<!-- * complex, -->
<!-- * ill-structured problems -->
<!-- * involving multiple (conflicting) criteria -->

---
layout: TRUE
# Algorithmique vs Statistique ?
## Breiman 2001: "**Statistical modeling: the two cultures**"`r AutoCite(myBib, "breiman2001statistical", .opts = list(cite.style = 'alphabetic'))`

--
.pull-left[
.center[
![:scale 100%](images/breiman-2cultures.png)
.content-box-yellow[
.small[
modélisation statistique<br>vs<br> modélisation mécanistique]]
]]

???
- data-model: les données sont générées par un modèle de donnée stochastique
- algorithmic-model: le mécanisme aboutissant aux données est traité comme inconnu

- culture algorithmique = un 3ème front dans la controverse fréquentiste vs bayésien

--
.pull-right[
Plusieurs cultures:
- quantiles
- méthodes de maximum d'entropie
- méthodes "*robustes*"
- méthodes bayésiennes
- ...
]

???
Chaque statisticien d'une chapelle se demandant pourquoi tous ne pensent pas comme lui


---
layout: false
# Apprentissage automatique / Machine Learning ?
.content-box-blue[
Machine Learning (ML) is a research field grounding in computer science that concentrates on induction algorithms and on other algorithms that can be said to ‘learn’
]

.content-box-blue[
"*A computer program is said **to learn from experience** .hlb[E] with respect to some class of **tasks** .hlb[T] and **performance measure** .hlb[P], if its performance at tasks in T, as measured by P, improves with experience E*" Mitchell, 1997 in Bierneat 2015
]


---
layout: false
class: inverse center middle text-white

.font200[Quelques exemples d'application <br>en environnement]

---
# SoilGrids250m
```{r, echo=FALSE, out.width='100%', fig.align='center'}
knitr::include_graphics("images/article-soilgrids250m.png")
```

---
# SoilGrids250m

<iframe src="https://soilgrids.org" width="100%"
align="center" height="400" scrolling="auto" frameborder="0"
allowtransparency allowfullscreen style="border:none;"></iframe>

.footnote[<https://www.isric.org/explore/soilgrids><br><https://soilgrids.org/>]

---
# SoilGrids250m
```{r, echo=FALSE, out.width='70%', fig.align='center'}
knitr::include_graphics("images/workflow-soilgrids250m.png")
```

.footnote[Fig 5 de Hengl **et al.** 2017]

---
# Predictive Soil Mapping with R
<iframe src="https://soilmapper.org/" width="100%"
align="center" height="400" scrolling="auto" frameborder="0"
allowtransparency allowfullscreen style="border:none;"></iframe>

---
# CarHab2: Cartographie prédictive des Habitats
.center[
.content-box-green[
$Pr(Y)=f(Relief,Bioclimat,Lithologie)$
]]

???
- la différence entre une cartographie *effective* et *prédictive* tient à l'extrapolation d'un modèle à tout un masque géographique sous l'hypothèse d'un groupe de conditions bioclimatiques et biophysiques *NON PERTURBEES* ie *CONSTANTES*

- $Y$ = la variable cible
- $Y$ peut être un type de végétation ou un biotope
- $Y$ a donc un nombre fini d'états = un problème de classification

--
.left-column[
.tiny[
- .hlb[T]: classification de types de végétation ou de biotopes
- .hlb[P]: % de pixels bien assignés au type
- .hlb[E]: apprentissage sur BDD tirée de l'expérience des botanistes des réseaux de CBN & Muséum NHN
]]
.right-column[
![:scale 60%](images/hengl2018RF-fig1.png)
]

.footnote[Fig 1 de Hengl **et al.** 2018 `r AutoCite(myBib, "Hengl2018", .opts = list(super = FALSE, cite.style = "alphabetic"))`]

---
# Ex: séries de végétations du Cher (18)

<iframe src="https://websig.univ-st-etienne.fr/cyrille/index.php/view/embed/?repository=cher&project=cherV4&layers=B0TTT&bbox=28038.760311%2C5810124.166689%2C530077.162018%2C6072761.79584&crs=EPSG%3A3857"
 width="100%" align="center" height="400" scrolling="auto" frameborder="0" allowtransparency allowfullscreen style="border:none;"></iframe>

---
# A la recherche d'une carte "*d'erreur*"
## quelle carte compagne ?
.left-column[
42 classes de végétation: un support (votes de l'algorithme) de moins de 60% apparaît dramatique mais un indice de Shannon (degré de confusion entre les classes) de 60% indique une faible confusion. 
]

.right-column[
![](images/carhab-cherV4_incetitudes.png)
]

---
layout: true
# Défis: dériver l'erreur de prédiction ...
## ... aux nouveaux emplacements ...

---

.left-column[
... dans:
- .hlb[l'espace]
]

.right-column[
![:scale 60%](images/Roberts2017-fig1-spatial.png)
]

---

.left-column[
... dans:
- l'espace
- .hlb[le temps]
]

.right-column[
![:scale 60%](images/Roberts2017-fig1-spatial-temporal.png)
]

---

.left-column[
... dans:
- l'espace
- le temps
- .hlb[d'autres structures de dépendence]
]

.right-column[
![:scale 60%](images/Roberts2017-fig1-spatial-temporal-autre.png)
]

---

.left-column[
... dans:
- l'espace
- le temps
- d'autres structures de dépendence
- d'autres structures .hlb[hiérarchisées: ex phylogénie]
]

.right-column[
![:scale 60%](images/Roberts2017-fig1.png)

.footnote[
Fig1 Roberts et al. 
`r AutoCite(myBib, "Roberts_2017", .opts = list(super = FALSE, cite.style = "alphabetic"))`
]
]

---
layout: false
# Défis: dériver l'erreur de prédiction ...
## ... côté outils ...
### => validation tenant compte du contexte spatial

.left-column[
.center[
- ![:scale 50%](images/logo-blockCV.png)
]
.center[
- `R::mlr`
]]

.right-column[
- blockCV: an `r icon::fa_r_project(colour = "#384CB7")` package for generating spatially or environmentally separated folds for k-fold cross-validation of species distribution models
`r AutoCite(myBib, "Valavi2018", .opts = list(super = FALSE, cite.style = "alphabetic"))`


![:scale 70%](images/capture-geocomputation-spatialcv.png)
]

---
layout: true
# Défis: dériver l'erreur de prédiction ...
## ... côté philo / éthique de la prédiction...

.content-box-red[.center[
"* **Cacher** l'incertitude pour éviter une mauvaise utilisation ? ou<br>**la communiquer** pour maximiser l'appropriation des résultats d'expertise?*" `r AutoCite(myBib, "bez", .opts = list(super = FALSE, cite.style = "alphabetic"))`
]]

---

.center[
![:scale 40%](images/risque.png)]
]

---

.center[
![:scale 50%](images/risque-lesautres.png)]
]


---
layout: false

class: fullscreen, inverse, top, center, text-white
background-image: url("images/VisionMachine_dark.jpg.png")
background-size: 80%
.font200[
** explicabilité : le compromis<br>entre précision et simplicité / interprétabilité<br>du modèle**
]

---
layout: true
# Défis: explicabilité / interprétabilité
## Pour quoi faire ?: pourquoi le modèle prédit ce qu'il prédit ?!

---

.pull-right[
![:scale 55%](images/iml.png)
]

???
- Vous pouvez remplacer **Humain** par **Financeur** ou **Décideur** :-)
- Si humain = chercheur

---

.pull-left[
- pour **.hlb[E/T/P]** où IA **.hlb[<]** Humain, alors: identifier les échecs de la machine
]


.pull-right[
![:scale 55%](images/iml.png)
]


---

.pull-left[
- pour **E/T/P** où IA **<** Humain, alors: identifier les échecs de la machine
- pour **.hlb[E/T/P]** où IA **.hlb[~]** Humain, alors: faire augmenter la confiance chez l'humain utilisateur
]

.pull-right[
![:scale 55%](images/iml.png)
]



---

.pull-left[
- pour **E/T/P** où IA **<** Humain, alors: identifier les échecs de la machine
- pour **E/T/P** où IA **~** Humain, alors: faire augmenter la confiance chez l'humain utilisateur
- pour **.hlb[E/T/P]** où IA **.hlb[>]** Humain, alors: faire augmenter la confiance chez l'humain utilisateur
]

.pull-right[
![:scale 60%](images/iml.png)
]

---
layout: false
# Défis: explicabilité / interprétabilité ...
## ... du côté des réseaux de neurones ? 

.tiny[L'algorithme devient capable de localiser ce qui l'a conduit (feature dans l'image) à assigner l'image à une classe. Il devient possible d'évaluer la confiance à mettre dans l'algorithme s'il est BON et sait montrer comment il a fait `r AutoCite(myBib, "selvaraju2017grad", .opts = list(super = FALSE, cite.style = "alphabetic"))`
]

.pull-left[
![:scale 50%](images/capture-selvaraju2017grad_dogcat.png)]
.pull-right[
![:scale 95%](images/capture-selvaraju2017grad_cavalier.png)]


.tiny[
Ca a l'air super mais... HELP ! 
- traduire ça en paramètres biophysiques et types de végétations ?
- aussi simple pour "convaincre" l'expert botaniste ?
]

---
# Google Earth Engine

.content-box-yellow[
*[...] an integrated platform **designed to empower** not only traditional remote sensing scientists, but also **a much wider audience** that lacks the technical capacity needed to utilize traditional supercomputers or large-scale commodity cloud computing resources.* `r AutoCite(myBib, "Gorelick2017", .opts = list(super = FALSE, cite.style = "alphabetic"))`
]
.content-box-yellow[
*The overarching goal of Earth Engine is to make progress on society's biggest challenges by making it not just possible, but easy, to monitor, track and manage the Earth's environment and resources. Doing so requires providing access not just to vast quantities of data and computational power, but also **increasingly sophisticated analysis techniques**, and **making them easy to use**.*
]

--
Zéro occurrence des mots ***fiabilité***, ***incertitude*** dans le papier publié dans **Remote Sensing of Environment** `r AutoCite(myBib, "Gorelick2017", .opts = list(super = FALSE, cite.style = "alphabetic"))`...

---
# Google Earth Engine
<iframe src="https://earthengine.google.com/case_studies/" width="100%"
align="center" height="400" scrolling="auto" frameborder="0"
allowtransparency allowfullscreen style="border:none;"></iframe>

.footnote[<https://earthengine.google.com/>]

---
layout: true
# Défis: explicabilité / interprétabilité ...
## De la black-box à la glass-box

---

.content-box-blue[
*Most of all, rising legal and privacy aspects, e.g. with the new European General Data Protection Regulations GDPR, make black-box approaches difficult to use, because they often are not able to explain .hlb[why a decision has been made], e.g. **why two objects are similar**.*
 `r AutoCite(myBib, "Holzinger2017", .opts = list(super = FALSE, cite.style = "alphabetic"))`]

---

```{r, echo=FALSE, out.width='70%', fig.align='center'}
knitr::include_graphics("images/iML_Holzinger2017.png")
```

- => ne pas inclure l'humain seulement lors de la sélection des données ou des variables mais **aussi durant la phase d'apprentissage en interaction directe** avec  l'algorithme

???
Approche Humain est dans la boucle

--
- => plus d'un humain entre dans la boucle:
  - études contributives et participatives
  - ludification

--
- .content-box-red[Quelle interface pour les données spatialisées ? Intégration avec les outils géomatiques ?]


---
### Réflexions en médecine pour coupler ML et Prise de Décision
.left-column[
.small[
- A: premier stade : l'humain joue le rôle de l'algorithme
- B: retour à l'utilisateur (modèle très rapide)
- C: collaboration: l'humain modifie l'algorithme durant son exécution
]]
.right-column[
![:scale 80%](images/capture-interactiveML.png)
`r AutoCite(myBib, "holzinger2016machine", .opts = list(super = FALSE, cite.style = "alphabetic"))`
]

???
De gauche à droite:
- data
- algorithme
- modèle

---
layout: true
# TransAlgo
.small[
.content-box-yellow[
"*Plateforme scientifique pour le développement de la transparence et de la redevabilité des algorithmes et des données*"
]]

---

![:scale 70%](images/capture-transalgo.png)

---
.left-column[
### Recherches sur le site:
]
.right-column[
```{r echo=FALSE, fig.cap='consultation le 09/01/20', fig.width=10}
library(tidyverse)

data_transalgo <- tibble(item = ordered(x=c("marketing","image", "commerce", "spatial", "environnement","géographie"),levels=c("marketing","image", "commerce", "spatial", "environnement","géographie")),hits = c(75,72,31,3,2,0))

data_transalgo %>%
  ggplot(aes(x=item,y=hits))+
  geom_col()+
  coord_flip()+
  ggtitle("Recherche du type: image site:https://www.transalgo.org/`")
```
]

---
layout: false
# TransAlgo
<iframe src="https://www.transalgo.org/category/domaine-d-application/" width="100%"
align="center" height="400" scrolling="auto" frameborder="0"
allowtransparency allowfullscreen style="border:none;"></iframe>

---
layout: false
class: inverse center middle text-white

.font200[Côté outils<br>Interfaces de programmation ML<br>sous<br>![:scale 25%](images/logo-R.png)]

---
# Interfaces ou meta paquets
\>**500** bibliothèques individuelles pour des tâches de **classification**, **régression** , **clustering** etc...

.pull-left[
![:scale 70%](images/outils-bordel.jpg)
]

--
.pull-right[
![:scale 70%](images/outils-ranges.jpg)
]

---
layout: true

# Unification des tâches quel que soit l'algorithme


---

.pull-left[
.tiny[
- pré-traitement des variables (normalisation, recodage, binarisation, synthèse (ACP)...)
- extraction de variables
- sélection de variables
]]

.pull-right[
.pull-left[
```{r echo=FALSE, fig.width=6, fig.asp=0.6, out.width="60%"}
knitr::include_graphics("images/mlr-feature_selection.png")
```
]
.pull-right[
```{r echo=FALSE, fig.width=6, fig.asp=0.6, out.width="60%"}
knitr::include_graphics("images/mlr-feature_filtering.png")
```
]]

---
.pull-left[
.tiny[
- pré-traitement des variables (normalisation, recodage, binarisation, synthèse (ACP)...)
- extraction de variables
- sélection de variables
- imputation
- partitionnement des données (validation, test, train)
- exploration des hyper-paramètres
]]

.pull-right[
```{r echo=FALSE, fig.width=6, fig.asp=0.6, out.width="30%"}
knitr::include_graphics("images/mlr-hyperparameters.png")
```
]

---
.pull-left[
.tiny[
- pré-traitement des variables (normalisation, recodage, binarisation, synthèse (ACP)...)
- extraction de variables
- sélection de variables
- imputation
- partitionnement des données (validation, test, train)
- exploration des hyper-paramètres
- entraînement du modèle
- prédiction utilisant un modèle
]]
.pull-right[
```{r echo=FALSE, fig.width=6, fig.asp=0.6, out.width="30%"}
knitr::include_graphics("images/mlr-training.png")
```
```{r echo=FALSE, fig.width=6, fig.asp=0.6, out.width="30%"}
knitr::include_graphics("images/mlr-predict.png")
```
]

---
.pull-left[
.tiny[
- pré-traitement des variables (normalisation, recodage, binarisation, synthèse (ACP)...)
- extraction de variables
- sélection de variables
- imputation
- partitionnement des données (validation, test, train)
- exploration des hyper-paramètres
- entraînement du modèle
- prédiction utilisant un modèle
- mesurer la performance
- ré-échantillonnage
- parallélisation
- benchmarking de modèles
- visualisation (de chacune des étapes précédentes): ex: importance des variables
- ensembling
]]

---
.pull-left[
.tiny[
- pré-traitement des variables (normalisation, recodage, binarisation, synthèse (ACP)...)
- extraction de variables
- sélection de variables
- imputation
- partitionnement des données (validation, test, train)
- exploration des hyper-paramètres
- entraînement du modèle
- prédiction utilisant un modèle
- mesurer la performance
- ré-échantillonnage
- parallélisation
- benchmarking de modèles
- visualisation (de chacune des étapes précédentes): ex: importance des variables
- ensembling
]]

.pull-right[
.content-box-red[
- besoin de standardisation de la syntaxe pour ces opérations
- => "***meta-package***"]]


---
layout: false
# Deux principaux paquets
## `R::caret`
.left-code[
"*The  *`caret` *package (**C**lassification **A**nd **RE**gression **T**raining) is a set of functions that attempt to streamline the process for creating predictive models.*"

.footnote[<https://topepo.github.io/caret/index.html>]
]
.pull-right[
![:scale 50%](images/book-kuhn.png)
]

---
# Deux principaux paquets
## `R::mlr` (et `R::mlr3`)
.center[
![:scale 70%](images/mlr3verse.svg)
]

---
# D'autres interfaces ~ meta
- $h_{2}0$
- CORELeaRn
- RWeka (langage Weka)
- SuperLearner
- ...
- et bien sûr dans d'autres langages tel `Python::scikitlearn`


---
# Des paquets pour l'interprétation des modèles ML...
.center[
![:scale 20%](images/logo-dalex.png)
![:scale 20%](images/logo-iml.png)
![:scale 20%](images/logo-lime.png)
]

---
# Pour finir


- Les approches dite "*génératives*" pourraient permettre d'échapper à la dichotomie déduction/induction
  - ex: celle des "*Modèles Hiérarchiques Bayesiens (MHB)* [qui peuvent] *réconcilier la modélisation statistique avec la modélisation mécaniste*" `r AutoCite(myBib, "robert2016reasoning", .opts = list(super = FALSE, cite.style = "alphabetic"))`
  - ex: simulation basée sur les agents
  - ex: GAN (DeepL Generative Adversarial Networks)
  
- tellement d'outils... .hlb[model mining ?] ^^'

- attention à l'encapsulation des algorithmes et de leurs paramètres (cf présentation de POM)
- sous `r icon::fa_r_project(colour = "#384CB7")` les implémentations 'spatiales' pour ML sont assez récentes (2018)
- un lexique français-anglais pour naviguer ![:scale 20%](images/logo-datafranca.png)

--
- MERCI !

---
# Références
.tiny[
```{r refs, echo=FALSE, results="asis"}
# Note: alterner les compilations en c hangeant bib.style="numeric" en bib.style="alphabetic" pour faire apparâitre toutes les références
PrintBibliography(myBib,.opts=list(bib.style="alphabetic"))
```
]